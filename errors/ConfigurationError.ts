class ConfigurationError extends Error {
  constructor(message: string, serviceOrConfigName: string) {
    const fullMessage = `Unable to configure ${serviceOrConfigName} - ${message}`;

    super(fullMessage);
    this.name = 'ConfigurationError';
  }
}

export default ConfigurationError;
