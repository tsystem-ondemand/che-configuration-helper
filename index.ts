import ConfigurationError from './errors/ConfigurationError';
import EnvVariablesMap from './types/EnvVariablesMap';
import EnvVariablesResult from './types/EnvVariablesResult';
import VariableOptions from "./types/VariableOptions";

export default class ConfigurationHelper {
  static getEnvVariables<T = EnvVariablesResult>(
    envVariablesMap: EnvVariablesMap,
    serviceOrConfigName = 'Another Cherehapa service',
  ): T {
    const processItem = ([configItemName, options]: [string, VariableOptions]): any => {
      const {
        name: envName,
        defaultValue,
        transform,
      } = options;

      let value;

      value = process.env[envName];

      if (value === undefined) {
        value = defaultValue;
      }

      if (transform) {
        try {
          value = transform(value);
        } catch (e) {
          const message = `An error occurred during transformation of ${configItemName}`;
          throw new ConfigurationError(message, serviceOrConfigName)
        }
      }

      if (value === undefined || Number.isNaN(value)) {
        throw new ConfigurationError(`process.env.${envName} is empty`, serviceOrConfigName);
      }

      return { [configItemName]: value };
    };

    return Object.entries(envVariablesMap)
      .reduce((acc, item) => ({ ...acc, ...processItem(item) }), {}) as T;
  }
}
